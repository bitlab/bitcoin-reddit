CLIENT_ID="<client_id>"
CLIENT_SECRET="<client_secret>"
USERNAME="<username>"
PASSWORD="<password>"

import praw
import docx
import argparse
import sys
import pprint
from docx.enum.style import WD_STYLE_TYPE
import datetime
import csv
import os

def parse_arguments():
	parser = argparse.ArgumentParser(description='Scrape and parse the Bitcoin reddit')

	parser.add_argument('-s', '--subreddit', action='store', default='Bitcoin')
	parser.add_argument('-n', '--num', action='store', type=int, 
		help="Number of submissions to retrieve (Max 100)")
	parser.add_argument('-o', '--output', action='store', 
		help="Directory to store files in, one per submission")
	parser.add_argument('-a', '--after', action='store',
		help="Only show threads after this one (format: t1_<thead_id>)")
	parser.add_argument('-b', '--before', action='store',
		help="Only show threads before this one (format: t1_<thead_id>)")
	parser.add_argument('-l', '--listfile', action='store', default="threads.txt",
 		help="Name of file to save the thread list to inside the output directory (default: threads.txt) (appends if file already exists)")
	parser.add_argument('-f', '--flair', action='store_true', 
		help="Record flair for post authors and comment authors")

	group = parser.add_mutually_exclusive_group()
	group.add_argument('-c', '--controversial', action='store_const', dest='search', const='controversial')
	group.add_argument('-g', '--gilded', action='store_const', dest='search', const='gilded')
	group.add_argument('--hot', action='store_const', dest='search', const='hot')
	group.add_argument('--new', action='store_const', dest='search', const='new', help="(default)")
	group.add_argument('-r', '--rising', action='store_const', dest='search', const='rising')
	group.add_argument('--top', action='store_const', dest='search', const='top')
	group.add_argument('-i', '--id', action='store', 
		help="Retrieve a specific thread by ID number")
	group.add_argument('-t', '--threadlist', action='store',
		help="Retrieve threads from an existing list of threads in a file")

	parser.set_defaults(search='new', id=None)

	info = parser.parse_args()
	print(info)
	return info

def add_hyperlink(paragraph, url, text, color=None, underline=None):
    """
    A function that places a hyperlink within a paragraph object.

    :param paragraph: The paragraph we are adding the hyperlink to.
    :param url: A string containing the required url
    :param text: The text displayed for the url
    :return: The hyperlink object
    """

    # This gets access to the document.xml.rels file and gets a new relation id value
    part = paragraph.part
    r_id = part.relate_to(url, docx.opc.constants.RELATIONSHIP_TYPE.HYPERLINK, is_external=True)

    # Create the w:hyperlink tag and add needed values
    hyperlink = docx.oxml.shared.OxmlElement('w:hyperlink')
    hyperlink.set(docx.oxml.shared.qn('r:id'), r_id, )

    # Create a w:r element
    new_run = docx.oxml.shared.OxmlElement('w:r')

    # Create a new w:rPr element
    rPr = docx.oxml.shared.OxmlElement('w:rPr')

    # Add color if it is given
    if not color is None:
      c = docx.oxml.shared.OxmlElement('w:color')
      c.set(docx.oxml.shared.qn('w:val'), color)
      rPr.append(c)

    # Remove underlining if it is requested
    if not underline:
      u = docx.oxml.shared.OxmlElement('w:u')
      u.set(docx.oxml.shared.qn('w:val'), 'none')
      rPr.append(u)

    # Join all the xml elements together add add the required text to the w:r element
    new_run.append(rPr)
    new_run.text = text
    hyperlink.append(new_run)

    paragraph._p.append(hyperlink)

    return hyperlink

def print_comments(doc, comment, flair=False, level=1):
	if not hasattr(comment, 'author'):
		return
	author = str(comment.author)
	text = comment.body
	if level > 1: 
		if level <= 3:
			style = "List Bullet %d" % level
		else:
			style = "List Bullet 3" # Max out at level 3
	else:
		style = "List Bullet"
	p = doc.add_paragraph(style=style)
	if level > 3:
		p.add_run("(%d)  " % level).italic = True
	p.add_run(author).bold = True
	if flair:
		p.add_run(" (")
		p.add_run(comment.author_flair_text).italic = True
		p.add_run(") ")
	p.add_run(": ")
	p.add_run(text)
	for reply in comment.replies:
		print_comments(doc, reply, flair, level+1)

def add_styles(doc):
	styles = doc.styles
	for i in range(6,10):
		style = styles.add_style('List Bullet %d' % i, WD_STYLE_TYPE.PARAGRAPH)
		style.base_style = styles['List Bullet 3']
	#print(style.ilvl)
	return doc

def save_thread_to_file(directory, filename, thread, flair):
	print("Saving thread %s (%d comments)" % (thread.id, thread.num_comments))
	document = docx.Document()
	document = add_styles(document)
	document.add_heading(thread.title, 0)
	p = document.add_paragraph("")
	p.add_run(str(thread.author)).bold = True
	if flair:
		p.add_run(" (")
		p.add_run(thread.author_flair_text).italic = True
		p.add_run(" )")
	document.add_paragraph(thread.selftext)
	if hasattr(thread, 'url'):
		p = document.add_paragraph("URL: ")
		add_hyperlink(p, text=thread.url, url=thread.url, color='Blue')
	p = document.add_paragraph("Permalink: ")
	plink = "https://www.reddit.com%s" % thread.permalink
	add_hyperlink(p, text=plink, url=plink, color="Blue")
	
	document.add_heading("Comments", 1)
	
	# Get list of (top-level) comments
	top_level_comments = thread.comments
	
	for comment in top_level_comments:
		print_comments(document, comment, flair)
	
	document.save("%s/%s" % (directory, filename))


def save_thread_list_to_file(threadlist, retrieved_at, directory, filename):
	full_filename = "%s/%s" % (directory, filename)
	with open(full_filename, "a") as f:
		csvwriter = csv.writer(f)
		for thread in threadlist:
			csvwriter.writerow([thread.id, retrieved_at.isoformat(), "https://www.reddit.com%s" % thread.permalink])
			

def get_thread_list_from_file(threadlist):
	with open(threadlist, "r") as f:
		id_list = []
		csvreader = csv.reader(f)
		for row in csvreader:
			id_list.append(row[0])
	id_list = set(id_list)
	out = []
	for id in id_list:
		thread = reddit.submission(id=id)
		out.append(thread)
	return out

info = parse_arguments()

reddit = praw.Reddit(client_id=CLIENT_ID, client_secret=CLIENT_SECRET, user_agent='BITLab Bitcoin reddit scraper', username=USERNAME, password=PASSWORD)

subr = reddit.subreddit(info.subreddit)

# If we specify an ID, then scrape ONLY that ID
if info.id:
	# thread = list(subr.search('id:%s' % info.id))[0]
	thread = reddit.submission(id=info.id)
	#print(thread.title)
	#pprint.pprint(vars(thread))
	save_thread_to_file(info.output, "%s.docx" % thread.id, thread)
	sys.exit(0)

# If no ID was specified, then do a search
	
# Get a list of submissions
search_func = getattr(subr, info.search)

get_type = info.search
if info.threadlist:
	submissions = get_thread_list_from_file(info.threadlist)
	get_type = "redownload"
elif info.after:
	submissions = search_func(limit=info.num, params={'after':info.after})
elif info.before:
	submissions = search_func(limit=info.num, params={'before': info.before})
else:
	submissions = search_func(limit=info.num)

retrieved_at = datetime.datetime.now()

if not info.output:
	info.output = "%s-%s" % (datetime.date.today().isoformat(), get_type)
if not os.path.exists(info.output):
	os.makedirs(info.output)
		

thread_list = []
for thread in submissions:
	thread_list.append(thread)
	save_thread_to_file(info.output, "%s.docx" % thread.id, thread, info.flair)

save_thread_list_to_file(thread_list, retrieved_at, info.output, info.listfile)













